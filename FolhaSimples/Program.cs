﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaMetodos
{
    class Program
    {
        
        static void Main(string[] args)
        {
            String nome;
            float valorHora = 0;
            int horas = 0;
            int opc;
		    float bruto, ir, inss, fgts;
		    bool leitura=false;
		    do{
			    Console.WriteLine("\n\n1 - Ler dados");
			    Console.WriteLine("2 - Calcular salário líquido");
			    Console.WriteLine("3 - Sair");
                Console.Write("Opção: ");
			    opc = int.Parse(Console.ReadLine());
			    switch(opc){
				    case 1:
					    Console.Write("\nNome: ");
                        nome = Console.ReadLine();
                        Console.Write("Horas trabalhadas: ");
                        horas = int.Parse(Console.ReadLine());
                        Console.Write("Valor da hora: ");
                        valorHora = float.Parse(Console.ReadLine());
					    leitura = true;
					    break;
				    case 2:
					    if(leitura){
                            bruto = horas * valorHora;
                            if (bruto <= 1372.81)
                            {
                                ir = 0;
                            }
                            else
                            {
                                if (bruto <= 2743.25)
                                {
                                    ir = (float)((bruto * .15) - 205.92);
                                }
                                else
                                {
                                    ir = (float)((bruto * .275) - 548.82);
                                }
                            }

                            if (bruto <= 868.29)
                            {
                                inss =  (float)(bruto * 0.08);
                            }
                            else
                            {
                                if (bruto <= 1447.14)
                                {
                                    inss = (float)(bruto * 0.09);
                                }
                                else
                                {
                                    if (bruto <= 2894.28)
                                    {
                                        inss = (float)(bruto * 0.11);
                                    }
                                    else
                                    {
                                        inss = (float)318.37;
                                    }
                                }
                            }
                            fgts = (float)(bruto * 0.08);
						    Console.WriteLine("\n\nSalário bruto: " + bruto.ToString("C2"));					
						    Console.WriteLine("INSS: " + inss.ToString("C2"));
						    Console.WriteLine("IR: " + ir.ToString("C2"));
						    Console.WriteLine("FGTS: " + fgts.ToString("C2"));
                            Console.WriteLine("Salário líquido: " + (bruto - inss - ir).ToString("C2"));
					    }
					    else{
                            Console.WriteLine("É necessário realizar a leitura dos dados.");
					    }
					    break;
			    }
		    }while(opc != 3);
        }

    }
}
